# Ansible role: ovirt-cloud-init

Customize VMs in a oVirt infrastructure with cloud-init.

## Requirements

None.

## Role variables

Available variables are listed below, along with the default values (see `defaults/main.yml`):

```yaml
ovirt_url: https://ovirt.example.org/ovirt-engine/api
```

The URL of the oVirt API to use.

```yaml
ovirt_login:
  username: user@realm
  password: Pa$sw0rd!
```

Set the credentials to connect oVirt API. The username should specify the realm.

```yaml
ovirt_vms: []
```

Set a list with the VMs with desired hostnames to be configured in the oVirt infrastructure. Each VM must have 'cloud_init_configuration' list of cloud-init configuration attributes. For example:

```yaml
ovirt_vms:
  - name: VM-example-1
    hostname: vm-example-01
    cloud_init_configuration:
      users:
        - default
        - name: ansible
          gecos: Ansible User
          shell: /bin/bash
          primary_group: ansible
          sudo: ALL=(ALL) NOPASSWD:ALL
          groups: users,sudo
          lock_passwd: true
      write_files:
      # ...
  - name: VM-example-2
    hostname: vm-example-02
    cloud_init_configuration:
      users:
        - default
        - name: ansible
          gecos: Ansible User
          shell: /bin/bash
          primary_group: ansible
          sudo: ALL=(ALL) NOPASSWD:ALL
          groups: users,sudo
          lock_passwd: true
      packages:
      # ...
```

## Author

Alberto Sosa (info@albertososa.es)
